namespace dotapi;

public class UserModel
{
    public string? name { get; set; }

    public string? lastname { get; set; }

    public int age { get; set; }

    public string? description { get; set; }

}
