using Microsoft.AspNetCore.Mvc;
using Dapper;
using MySql.Data.MySqlClient;

namespace dotapi.Controllers;

[ApiController]
[Route("[controller]")]
public class UsersController : ControllerBase
{

    private readonly IConfiguration _config;

    public UsersController(IConfiguration config)
    {
        _config = config;
    }

   [HttpGet]
    public  async Task<ActionResult<List<UserModel>>> GetUsers(){

     using var connection = new MySqlConnection(_config.GetConnectionString("MysqlConnection"));
     var users =  await connection.QueryAsync<UserModel>("select * from users");
     return Ok(users);
    }

    [HttpGet("{idUser}")]
    public  async Task<ActionResult<List<UserModel>>> GetUser(int idUser){

     using var connection = new MySqlConnection(_config.GetConnectionString("MysqlConnection"));
     var users =  await connection.QueryFirstAsync<UserModel>("select * from users where id = @id", new {id = idUser});
     return Ok(users);
    }

    [HttpPost]
    public  async Task<ActionResult<List<UserModel>>> CreateUser(UserModel user){

     using var connection = new MySqlConnection(_config.GetConnectionString("MysqlConnection"));
     await connection.ExecuteAsync("insert into users (name,lastname,age,description) values (@name,@lastname,@age,@description)", user);
       return Ok(new { status = true, message = "registro exitoso"});
    }

    [HttpPut("{idUser}")]
    public  async Task<ActionResult<List<UserModel>>> UpdateUser(UserModel user, int idUser){

     using var connection = new MySqlConnection(_config.GetConnectionString("MysqlConnection"));
     await connection.ExecuteAsync("update users set name = @name, lastname = @lastname, age = @age, description = @description where id = @id", new {name = user.name, lastname =user.lastname, age = user.age, description = user.description,  id = idUser});
       return Ok(new { status = true, message = "registro editado exitoso"});
    }

    [HttpDelete("{idUser}")]
    public  async Task<ActionResult<List<UserModel>>> DeleteUser(int idUser){

     using var connection = new MySqlConnection(_config.GetConnectionString("MysqlConnection"));
     await connection.ExecuteAsync("delete from users where id = @id", new {id = idUser});
        return Ok(new 
            { 
                status = true,
                message = "registro eleminado"
            });
    }

}